from django.db import models

class DataForSearch(models.Model):
    city = models.CharField(max_length=120, default=None)
    product = models.CharField(max_length=120, default=None)


class ShopData(models.Model):
    title = models.CharField(max_length=150, default=None)
    price = models.CharField(max_length=150, default=None)
    currency = models.CharField(max_length=3, default=None)
    address = models.CharField(max_length=150, default=None)
    date = models.CharField(max_length=150, default=None)
    url = models.CharField(max_length=150, default=None)