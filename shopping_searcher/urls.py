from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.Shopping_Searcher, name='shop'),
]