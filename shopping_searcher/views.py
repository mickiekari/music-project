from django.shortcuts import render

from .forms import DataForSearchForm, ShopDataForm
from shopping_searcher.searcher import ShopingSearcher

def Shopping_Searcher(request):
    template_name = 'shopping_searcher/shop.html'

    if request.method == 'POST':
        form = DataForSearchForm(data=request.POST)
        if form.is_valid():
            city = form.cleaned_data['city']
            product = form.cleaned_data['product']

            searcher = ShopingSearcher(city, product)

            blocks = searcher.get_sorted_blocks()

            shoppingData = []

            for i in blocks:
                print(i)
                productData = [i.title,
                               i.price,
                               i.currency,
                               i.address,
                               i.date,
                               i.url]
                shoppingData.append(productData)

            return render(request, template_name, {'form': form, 'shoppingData': shoppingData})

    form = DataForSearchForm()

    return render(request, template_name, {'form': form})
