from django.forms import ModelForm, TextInput
from .models import DataForSearch, ShopData



class DataForSearchForm(ModelForm):
    class Meta:
        model = DataForSearch
        fields = ['city', 'product']

        widgets = {'city': TextInput(attrs={'class': 'form-control justify-content-center', 'id': 'city', 'type': 'name', 'placeholder': 'Город'}),
                  'product': TextInput(attrs={'class': 'form-control justify-content-center', 'id': 'product', 'type': 'name', 'placeholder': 'Название товара'})}


class ShopDataForm(ModelForm):
    class Meta:
        model = ShopData
        fields = ['title', 'price', 'currency', 'address', 'date', 'url']

        widgets = {'title': TextInput(attrs={'class': 'form-control ', 'id': 'title', 'type': 'name'}),
                   'price': TextInput(attrs={'class': 'form-control', 'id': 'price', 'type': 'name'}),
                   'currency': TextInput(attrs={'class': 'form-control', 'id': 'currency', 'type': 'name'}),
                   'address': TextInput(attrs={'class': 'form-control', 'id': 'address', 'type': 'name'}),
                   'date': TextInput(attrs={'class': 'form-control', 'id': 'date', 'type': 'date'}),
                   'url': TextInput(attrs={'class': 'form-control', 'id': 'url', 'type': 'name'})}