from django.apps import AppConfig


class ShoppingSearcherConfig(AppConfig):
    name = 'shopping_searcher'
