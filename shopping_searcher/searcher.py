from collections import namedtuple

import bs4
import requests
import datetime

InnerBlock = namedtuple('Block', 'title, price, currency, address, date, url')

class Block(InnerBlock):

    def __str__(self):
        return f'{self.title}\t{self.price}\t{self.currency}\t{self.address}\t{self.date}\t{self.url}'


class ShopingSearcher:
    city = ''
    productName = ''

    def __init__(self, city, productName):
        self.session = requests.Session()
        self.session.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36',
            'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7'
        }

        self.city = city.lower()
        self.productName = productName.lower()


    # Преобразовывает дату с сайта
    @staticmethod
    def parse_date(item: str):
        params = item.strip().split(' ')
        if len(params) == 2:
            day, time = params
            if day == 'Сегодня':
                date = datetime.date.today()
            elif day == 'Вчера':
                date = datetime.date.today() - datetime.timedelta(days=1)
            else:
                print('Непонятный день:', item)
                return
            time = datetime.datetime.strptime(time, '%H:%M').time()
            return datetime.datetime.combine(date=date, time=time)
        elif len(params) == 3:
            day, mounth_hru, time = params
            day = int(day)
            mounth_map = {
                'января': 1,
                'февраля': 2,
                'марта': 3,
                'апреля': 4,
                'мая': 5,
                'инюня': 6,
                'июля': 7,
                'августа': 8,
                'сентября': 9,
                'октября': 10,
                'ноября': 11,
                'декабря': 12
            }
            month = mounth_map.get(mounth_hru)
            if not month:
                print('Непонятный месяц:', item)
                return

            today = datetime.datetime.today()
            time = datetime.datetime.strptime(time, '%H:%M')
            return datetime.datetime(day=day,month=month, year=today.year, hour=time.hour, minute=time.minute)
        else:
            print('Непонятный формат даты', item)
            return


    # Возвращает html странницу в текстовом виде
    def get_page(self, page: int = None):
        params = {}
        if page and page > 1:
            params['p'] = page

        url = 'https://www.avito.ru/' + self.city + '/muzykalnye_instrumenty?radius=200&q=' + self.productName.replace(' ', '+')
        r = self.session.get(url, params=params)
        # print(r.text)
        return r.text


    # Парсинг блоков с товарами, возвращает список с информацией о товаре и ссылкой
    def parse_block(self, item):
        # Выбрать блок со ссылкой на товар
        url_block = item.select_one('a.link-link-39EVK.link-design-default-2sPEv.title-root-395AQ.iva-item-title-1Rmmj.title-listRedesign-3RaU2.title-root_maxHeight-3obWc')
        try:
            href = url_block.get('href')
        except:
            href = None

        if href:
            url = 'https://www.avito.ru' + href
        else:
            url = 'Ссылка на товар отсутствует'

        # Выбрать блок с названием товара
        title_block = item.select_one('h3')
        if self.productName in title_block.string.lower():
            title = title_block.string.strip()
        else:
            print('Товар не подходит под описание')
            return

        # Выбрать блок с ценой товара и валютой
        price_block = item.select_one('div.iva-item-priceStep-2qRpg span.price-price-32bra span.price-text-1HrJ_.text-text-1PdBw.text-size-s-1PUdo')
        price_block_str = price_block.get_text('\n')

        if price_block_str != 'Цена не указана':
            if price_block_str == 'Бесплатно':
                price = 0
                currency = '0'
            else:
                price_block_str = list(filter(None, map(lambda i: i.strip(), price_block_str.split('\n'))))
                price = price_block_str[0]
                currency = price_block_str[1]
        else:
            print('проблемы с ценой')
            return

        # Выбрать блок с адресом
        address_block = item.select_one('div.geo-root-1pUZ8.iva-item-geo-1Ocpg span.geo-address-9QndR.text-text-1PdBw.text-size-s-1PUdo')
        if address_block != None:
            address = address_block.string.strip()
        else:
            address_block = item.select_one('div.geo-georeferences-3or5Q.text-text-1PdBw.text-size-s-1PUdo')
            if address_block != None:
                address = address_block.string.strip()
            else:
                address = 'Адрес не указан'

        # Выбрать блок с датой размещения
        date = None
        date_block = item.select_one('div.date-text-2jSvU.text-text-1PdBw.text-size-s-1PUdo.text-color-noaccent-bzEdI')
        absolute_date = date_block.string.strip()
        if absolute_date:
            # date = self.parse_date(item=absolute_date)
            date = absolute_date

        return Block(
            url=url,
            title=title,
            price=price,
            currency=currency,
            address=address,
            date=date
        )


    # Возвращает отсортированный список товаров по увеличению цены
    def get_sorted_blocks(self):
        blocks = []
        for i in range(1):
            html = self.get_page(page=i)
            soup = bs4.BeautifulSoup(html, 'lxml')

            countainer = soup.select('div.iva-item-content-m2FiN')
            for item in countainer:
                block = self.parse_block(item=item)
                if block != None:
                    blocks.append(block)

        blocks.sort(key=lambda block: block[1])
        return blocks


def main():
    # p = ShopingSearcher('chelyabinsk', 'звуковая карта')
    p = ShopingSearcher('zlatoust', 'звуковая карта')
    l = p.get_sorted_blocks()
    for i in l:
        print(i)


if __name__ == '__main__':
    main()
