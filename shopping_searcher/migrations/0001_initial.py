# Generated by Django 3.1.6 on 2021-05-20 07:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DataForSearch',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('city', models.CharField(default=None, max_length=120)),
                ('product', models.CharField(default=None, max_length=120)),
            ],
        ),
        migrations.CreateModel(
            name='ShopData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default=None, max_length=150)),
                ('price', models.CharField(default=None, max_length=150)),
                ('currency', models.CharField(default=None, max_length=3)),
                ('address', models.CharField(default=None, max_length=150)),
                ('date', models.CharField(default=None, max_length=150)),
                ('url', models.CharField(default=None, max_length=150)),
            ],
        ),
    ]
