from django.contrib.auth.models import User
from .models import Account
from django.forms import ModelForm, TextInput, PasswordInput
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django import forms


class UserRegisterForm(UserCreationForm):
    username = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'username', 'type': 'username', 'placeholder': 'Логин'}
        )
    )

    email = forms.EmailField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'email', 'type': 'email', 'placeholder': 'name@example.com'}
        )
    )

    firstname = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'firstname', 'type': 'name', 'placeholder': 'Имя'}
        )
    )

    lastname = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'lastname', 'type': 'name', 'placeholder': 'Фамилия'}
        )
    )

    password1 = forms.CharField(
        required=True,
        widget=forms.PasswordInput(
            attrs={'class': 'form-control', 'id': 'password1', 'type': 'password', 'placeholder': '••••••••'}
        )
    )

    password2 = forms.CharField(
        required=True,
        widget=forms.PasswordInput(
            attrs={'class': 'form-control', 'id': 'password2', 'type': 'password', 'placeholder': '••••••••'})
    )

    termsOfUse = forms.BooleanField(required=True)

    field_order = ['username', 'email', 'password1', 'password2']


class LoginForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ['username','password']
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'E-mail или логин'})
        self.fields['username'].label = False
        self.fields['password'].widget = forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'Пароль'})
        self.fields['password'].label = False


class RegistrationForm(ModelForm):
    class Meta:
        model = Account
        fields = ['name', 'surname', 'email', 'phone', 'password']

        widgets = {'name': TextInput(attrs={'class': 'form-control', 'id': 'name', 'type': 'name', 'placeholder': 'Имя'}),
                  'surname': TextInput(attrs={'class': 'form-control', 'id': 'surname', 'type': 'name', 'placeholder': 'Фамилия'}),
                  'email': TextInput(attrs={'class': 'form-control', 'id': 'email', 'type': 'email', 'placeholder': 'name@example.com'}),
                  'phone': TextInput(attrs={'class': 'form-control', 'id': 'phone', 'type': 'phone', 'placeholder': '+7**********'}),
                  'password': TextInput(attrs={'class': 'form-control', 'id': 'password', 'type': 'password', 'placeholder': '••••••••'})}
