from django.contrib.auth import login
from django.shortcuts import render, redirect
from django.views.generic import DetailView

from .forms import UserRegisterForm, LoginForm
from django.contrib.auth.models import User
from MusicPlayer.models import Song
from django.contrib import messages

from profiles.models import Profile


def MainView(request):
    template_name = 'core/homePage.html'

    return render(request, template_name)

def RegistrationView(request):
    errorMessage = "все нормально"
    template_name = 'core/regPage.html'

    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        username = request.POST.get('username')
        email = request.POST.get('email')

        if User.objects.filter(username=username).exists():
            messages.error(request, 'Имя пользователя уже занято')
            errorMessage = "Имя пользователя уже занято"
        elif User.objects.filter(email=email).exists():
            messages.error(request, 'Пользователь с данным адресом уже зарегестрирован')
            errorMessage = "Пользователь с такой почтой уже зарегестрирован"
        else:
            if form.is_valid() and form.cleaned_data['termsOfUse']:
                firstname = form.cleaned_data['firstname']
                lastname = form.cleaned_data['lastname']
                password = form.cleaned_data['password1']

                new_user = User.objects.create_user(username, email, password)
                new_user.first_name = firstname
                new_user.last_name = lastname
                new_user.save()

                new_profile = Profile.objects.create(user=new_user,
                                                     first_name=firstname,
                                                     last_name=lastname,
                                                     slug=username)
                new_profile.save()

                messages.success(request, 'Вы успешно зарегистрировались')
                return redirect('/login/')

    form = UserRegisterForm()

    context = {'form': form, 'errorMessage': errorMessage}

    return render(request, template_name, context)


def LoginView(request):
    template_name = 'core/authorizationPage.html'

    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)

            return redirect('/profile/' + user.username)
    else:
        form = LoginForm()

    return render(request, template_name, {'form': form})


def termsOfUse(request):
    template_name = 'core/termsOfUse.html'
    return render(request, template_name)

def PersonalPage(request):
    template_name = 'core/personalPage.html'

    return render(request, template_name)

def indexTest(request):
    template_name = 'core/testPage.html'

    return render(request, template_name)

def p_one(request):
    template_name = 'core/page_one.html'
    return render(request, template_name)

def p_two(request):
    template_name = 'core/page_two.html'
    return render(request, template_name)