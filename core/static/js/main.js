// Валидация логина
$(function () {
    $("#done").click(function () {

        var username = $("#username").val();
        var fail = "";

        if (username.length === 0) {
            fail = "Введите логин";
        }

        if (fail !== ""){
            $('#usernameMessageError').html(fail + "<label class='clear'></label>").show();
            return false;
        }
        else{
             $('#usernameMessageError').hide();
        }
    });
});

// Валидация email
$(function () {
    $("#done").click(function () {

        var email = $("#email").val();
        var fail = "";

        if (email.length === 0) {
            fail = "Введите введите email";
        }
        else if (email.split('@').length - 1 === 0 || email.split('.').length - 1 === 0) {
            fail = 'Вы ввели некорректный email';
        }

        if (fail !== ""){
            $('#emailMessageError').html(fail + "<label class='clear'></label>").show();
            return false;
        }
        else{
             $('#emailMessageError').hide();
        }
    });
});

// Валидация пароля
$(function () {
    $("#done").click(function () {

        var password1 = $("#password1").val();
        var password2 = $("#password2").val();
        var fail = "";

        if (password1.length < 6) {
            fail = "Введите минимум 6 символов";
        }
        else if (password2 !== password1)
        {
            fail = "Пароли не совпадают"
        }

        if (fail !== ""){
            $('#passwordMessageError').html(fail + "<label class='clear'></label>").show();
            return false;
        }
        else{
             $('#passwordMessageError').hide();
        }
    });
});

// Валидация имени пользователя
$(function () {
    $("#done").click(function () {

        var name = $("#firstname").val();
        var fail = "";

        if (name.length < 3) {
            fail = "Имя не меньше 3 символов";
        }

        for (var i = 0; i < name.length; i++){
            if (i === 0 && name[i] !== name[i].toUpperCase()) {
                fail = "Имя должно быть с заглавной буквы";
                break;
            }
            else if (i !== 0 && name[i] !== name[i].toLowerCase()){
                fail = "Все буквы, кроме первой должны быть маленькими";
                break;
            }
            else if (/^[\d]+$/.test(name[i])){
                fail = "Имя не должно состоять из цифр";
                break;
            }
        }

        if (fail !== ""){
            $('#firstnameMessageError').html(fail + "<label class='clear'></label>").show();
            return false;
        }
        else{
             $('#firstnameMessageError').hide();
        }
    });
});

// Валидация фамилии пользователя
$(function () {
    $("#done").click(function () {

        var surname = $("#lastname").val();
        var fail = "";

        if (surname.length < 3) {
            fail = "Фамилия не меньше 3 символов";
        }

        for (var i = 0; i < surname.length; i++){
            if (i === 0 && surname[i] !== surname[i].toUpperCase()) {
                fail = "Фамилия должна быть с заглавной буквы";
                break;
            }
            else if (i !== 0 && surname[i] !== surname[i].toLowerCase()){
                fail = "Все буквы, кроме первой должны быть маленькими";
                break;
            }
            else if (/^[\d]+$/.test(surname[i])){
                fail = "Фамилия не должна состоять из цифр";
                break;
            }
        }

        if (fail !== ""){
            $('#lastnameMessageError').html(fail + "<label class='clear'></label>").show();
            return false;
        }
        else{
             $('#lastnameMessageError').hide();
        }
    });
});

// Валидация номера телефона
$(function () {
    $("#done").click(function () {

        var phone = $("#phone").val();
        var fail = "";

        if (phone.length !== 12) {
            fail = "Введите 11 цифр";
        }

        for (var i = 0; i < phone.length; i++){
            if (i === 0 && phone[i] !== '+') {
                fail = "Номер должен начинаться со знака +";
                break;
            }
            else if (i !== 0 && !/^-{0,1}\d+$/.test(phone[i])){
                fail = "Номер должен состоять из цифр";
                break;
            }
        }

        if (fail !== ""){
            $('#phoneMessageError').html(fail + "<label class='clear'></label>").show();
            return false;
        }
        else{
             $('#phoneMessageError').hide();
        }
    });
});

$(function () {
    $("#search_user_button").click(function () {
        var val = document.getElementById('search_user_input').value;
		document.location.href = "http://127.0.0.1:8000/profile/" + val + "/";
    })
})

var audio = {
    init: function() {
    var $that = this;
        $(function() {
            $that.components.media();
        });
    },
    components: {
        media: function(target) {
            var media = $('audio.fc-media', (target !== undefined) ? target : 'body');
            if (media.length) {
                media.mediaelementplayer({
                    audioHeight: 40,
                    features : ['playpause', 'current', 'duration', 'progress', 'volume', 'tracks', 'fullscreen'],
                    alwaysShowControls      : true,
                    timeAndDurationSeparator: '<span></span>',
                    iPadUseNativeControls: true,
                    iPhoneUseNativeControls: true,
                    AndroidUseNativeControls: true
                });
            }
        },

    },
};

audio.init();

