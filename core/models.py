from django.db import models

class Account(models.Model):
    name = models.CharField(max_length=120, default=None)
    surname = models.CharField(max_length=120, default=None)
    email = models.CharField(max_length=120)
    phone = models.CharField(max_length=120)
    password = models.CharField(max_length=120)

    def __str__(self):
        return self.email