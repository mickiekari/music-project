from django.conf.urls import url
from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    url(r'^$', views.MainView, name='MainView'),
    url(r'^registration/$', views.RegistrationView, name='RegistrationFormView'),
    url(r'^login/?$', views.LoginView, name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='core/homePage.html'), name='logout'),
    url(r'^yourpage/$', views.PersonalPage, name='PersonalPage'),
    url(r'^test/$', views.indexTest, name='indexTest'),
    path('registration/termsofuse/', views.termsOfUse, name='terms-of-use'),

    path('password-reset/',
         auth_views.PasswordResetView.as_view(template_name='core/password_reset.html'),
         name='password_reset'),
    path('password-reset/done/',
         auth_views.PasswordResetDoneView.as_view(template_name='core/password_reset_sent.html'),
         name='password_reset_done'),
    path('reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name='core/password_reset_form.html'),
         name='password_reset_confirm'),
    path('reset/done/',
         auth_views.PasswordResetCompleteView.as_view(template_name='core/password_reset_done.html'),
         name='password_reset_complete'),

]