from django.db import models
from django.contrib.auth.models import User


class Song(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    artist = models.CharField(max_length=100)
    image = models.ImageField(upload_to='media/')
    audio_file = models.FileField(upload_to='media/')
    likes = models.PositiveIntegerField(default=0)
    feedback = models.CharField(default='Пока что никто не оставлял отзыва о песне', max_length=200, blank=True)

    def __str__(self):
        return self.title
