from django.shortcuts import render
from . models import Song

from profiles.forms import SongForm



def index(request):
    template_name = 'index.html'

    if request.method == 'POST':
        form = SongForm(data=request.POST)
        if form.is_valid():
            artist = form.cleaned_data['artist']
            title = form.cleaned_data['title']

            songs = []

            if artist and title:
                songs = Song.objects.filter(artist=artist, title=title)
            elif artist:
                songs = Song.objects.filter(artist=artist)
            elif title:
                songs = Song.objects.filter(title=title)

            return render(request, template_name, {'form': form, 'songs': songs})

    form = SongForm()

    return render(request, template_name, {'form': form})
