from django.contrib import messages
from django.views.generic import DetailView
from django.shortcuts import redirect, render

from .models import Profile
from MusicPlayer.models import Song
from .forms import AddSongForm, ProfileForm, AddFeedbackForm
from django.contrib.auth.models import User

from copyright_check import checker


class ProfileDetail(DetailView):
    """Профиль пользователя"""
    model = Profile
    context_object_name = 'profile'
    template_name = 'profiles/user_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileDetail, self).get_context_data(**kwargs)
        user = User.objects.get(username=kwargs.get('object'))
        context['Songs'] = Song.objects.filter(user=user).all()
        context['FormSongs'] = AddSongForm()
        return context

    def post(self, request, *args, **kwargs):
        print(kwargs)
        print(request.POST)

        return render(request, self.template_name)


class AddSong(DetailView):
    """Загрузка новой песни"""
    model = Profile
    context_object_name = 'profile'
    template_name = 'profiles/add_song.html'

    def get_context_data(self, **kwargs):
        context = super(AddSong, self).get_context_data(**kwargs)
        context['form'] = AddSongForm()
        return context

    def post(self, request, *args, **kwargs):
        form = AddSongForm(data=request.POST, files=request.FILES)

        if form.is_valid():
            user = User.objects.get(username=kwargs.get('slug'))
            title = form.cleaned_data['title']
            artist = form.cleaned_data['artist']
            image = form.cleaned_data['image']
            audio_file = form.cleaned_data['audio_file']

            if not Song.objects.filter(artist=artist, title=title).exists():   # and checker.checker(audio_file):
                new_song = Song.objects.create(user=user,
                                               title=title,
                                               artist=artist,
                                               image=image,
                                               audio_file=audio_file)
                new_song.save()

                Profile.objects.filter(user=user).update(
                    song_count=Profile.objects.get(user=user).song_count + 1
                )

                messages.success(request, 'Вы успешно загрузили аудиофайл')

            return redirect('/profile/' + kwargs.get('slug'))

        form = AddSongForm()
        return render(request, self.template_name, {'form': form})


class EditProfile(DetailView):
    """Редактирование профиля"""
    model = Profile
    context_object_name = 'profile'
    template_name = 'profiles/edit_profile.html'

    def get_context_data(self, **kwargs):
        context = super(EditProfile, self).get_context_data(**kwargs)
        context['form'] = ProfileForm()
        return context

    def post(self, request, *args, **kwargs):
        form = ProfileForm(data=request.POST, files=request.FILES)

        if form.is_valid():
            user = User.objects.get(username=kwargs.get('slug'))
            avatar = form.cleaned_data['avatar']
            phone = form.cleaned_data['phone']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            city = form.cleaned_data['city']
            country = form.cleaned_data['country']

            Profile.objects.filter(user=user).update(
                avatar=avatar,
                phone=phone,
                first_name=first_name,
                last_name=last_name,
                city=city,
                country=country
            )
            messages.success(request, 'Вы успешно изменили профиль')

            return redirect('/profile/' + kwargs.get('slug'))

        form = ProfileForm()
        return render(request, self.template_name, {'form': form})

class GiveFeedback(DetailView):
    """Отзыв о песне"""
    model = Profile
    context_object_name = 'profile'
    template_name = 'profiles/give_feedback.html'

    def get_context_data(self, **kwargs):
        context = super(GiveFeedback, self).get_context_data(**kwargs)
        context['form'] = AddFeedbackForm()
        return context

    def post(self, request, *args, **kwargs):
        form = AddFeedbackForm(request.POST)

        if form.is_valid():
            artist = form.cleaned_data['artist']
            title = form.cleaned_data['title']
            feedback = form.cleaned_data['feedback']
            feedback = kwargs.get('slug') + ': ' + feedback

            Song.objects.filter(artist=artist, title=title).update(
                feedback=feedback
            )

            return redirect('/profile/' + kwargs.get('slug'))
