from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


class Profile(models.Model):
    """Модель профиля пользователя"""
    user = models.OneToOneField(User, verbose_name="Пользователь", on_delete=models.CASCADE, blank=True)
    avatar = models.ImageField('Аватар', upload_to='avatar/', blank=True, null=True)
    phone = models.CharField('Телефон', max_length=25, blank=True)
    first_name = models.CharField('Имя', max_length=50, default='', blank=True)
    last_name = models.CharField('Фамилия', max_length=50, default='', blank=True)
    slug = models.SlugField('URL', max_length=50, default='', blank=True)
    city = models.CharField('Город', max_length=50, blank=True, default='Не указано')
    country = models.CharField('Страна', max_length=50, blank=True, default='Не указано')
    song_count = models.PositiveIntegerField('Колличество треков', default=0, blank=True)
    subscriptions = models.PositiveIntegerField('Колличество подписок', default=0, blank=True)
    subscribers = models.PositiveIntegerField('Колличество подписчиков', default=0, blank=True)

    def __str__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.first_name = self.user.first_name
        self.last_name = self.user.last_name
        self.slug = self.user.username

    def get_absolute_url(self):
        return reverse("profile-detail", kwargs={"slug": self.user.username})

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'
