from django import forms
from profiles.models import Profile
from MusicPlayer.models import Song


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('user', 'avatar', 'phone', 'first_name', 'last_name', 'slug', 'city', 'country')

    phone = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'phone', 'type': 'phone', 'placeholder': 'Телефон'}
        )
    )

    first_name = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'first_name', 'type': 'first_name', 'placeholder': 'Имя'}
        )
    )

    last_name = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'last_name', 'type': 'last_name', 'placeholder': 'Фамилия'}
        )
    )

    city = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'city', 'type': 'city', 'placeholder': 'Город'}
        )
    )

    country = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'country', 'type': 'country', 'placeholder': 'Страна'}
        )
    )


class AddSongForm(forms.ModelForm):
    class Meta:
        model = Song
        fields = ('title', 'artist', 'image', 'audio_file')

    title = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'title', 'type': 'title', 'placeholder': 'Название'}
        )
    )

    artist = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'artist', 'type': 'artist', 'placeholder': 'Исполнитель'}
        )
    )

    image = forms.ImageField()

    audio_file = forms.FileField()

class AddFeedbackForm(forms.ModelForm):
    class Meta:
        model = Song
        fields = ('artist', 'title', 'feedback')

    title = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'title', 'type': 'title', 'placeholder': 'Название'}
        )
    )

    artist = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'artist', 'type': 'artist', 'placeholder': 'Исполнитель'}
        )
    )

    feedback = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'feedback', 'type': 'feedback', 'placeholder': 'Напишите свой отзыв'}
        )
    )

class SongForm(forms.ModelForm):
    class Meta:
        model = Song
        fields = ('title', 'artist', 'image', 'audio_file')

    title = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'title', 'type': 'title', 'placeholder': 'Название'}
        )
    )

    artist = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'id': 'artist', 'type': 'artist', 'placeholder': 'Исполнитель'}
        )
    )

    image = forms.ImageField(required=False)

    audio_file = forms.FileField(required=False)
