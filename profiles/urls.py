from django.urls import path
from django.conf.urls.static import static

from . import views
from django.conf import settings

urlpatterns = [
    path("<slug>/", views.ProfileDetail.as_view(), name="profile-detail"),
    path("<slug>/addsong/", views.AddSong.as_view(), name="add-new-song"),
    path("<slug>/editprofile/", views.EditProfile.as_view(), name="edit-profile"),
    path("<slug>/givefeedback/", views.GiveFeedback.as_view(), name="give-feedback"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
