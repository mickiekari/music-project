import requests
import json
import time

def checker(filename):
    apikey = '01f309751fc353141217ade1bba728f8'
    url = 'https://audiotag.info/api'

    payload = {'action': 'identify', 'apikey': apikey}

    result = requests.post(url, data=payload, files={'file': open(filename, 'rb')})

    # print('первый ответ на запрос. получаем токен')
    # print(result.text)

    json_object = json.loads(result.text)

    token = json_object['token']

    # print('ждем и получаем ответ на запрос по токену')

    n = 1
    while n < 100:
        time.sleep(0.5)
        # print('request:%d'%(n))
        n += 1
        payload = {'action': 'get_result', 'token': token, 'apikey': apikey}
        result = requests.post(url, data=payload)
        json_object = json.loads(result.text)
        # print(json_object)
        if json_object['result'] != 'wait':
            break

    pretty_print = json.dumps(json_object, indent=4, sort_keys=True)
    print(json.loads(pretty_print)["result"])
    if json.loads(pretty_print)["result"] == 'not found':
        return True
    else:
        return False

def main():
    # filename = 'data/The Weeknd.mp3'
    filename = 'data/ROAD TO DESTINY.mp3'
    print(checker(filename))

if __name__ == '__main__':
    main()