from django.apps import AppConfig


class CopyrightCheckConfig(AppConfig):
    name = 'copyright_check'
